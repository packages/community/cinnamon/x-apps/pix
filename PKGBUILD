# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Contributor: Nate Simon <njsimon10@gmail.com>

pkgname=pix
pkgver=3.4.5
pkgrel=1
pkgdesc="Image viewer and browser utility."
arch=('x86_64')
url="https://github.com/linuxmint/pix"
license=('GPL-2.0-or-later')
groups=('x-apps')
depends=(
  'gsettings-desktop-schemas'
  'gst-plugin-gtk'
  'gst-plugins-base-libs'
  'gstreamer'
  'gtk3'
  'hicolor-icon-theme'
  'libheif'
  'libjxl'
  'libsecret'
  'libpng'
  'libsoup'
  'librsvg'
  'libwebp'
  'libx11'
  'webkit2gtk-4.1'
  'xapp'
)
makedepends=(
  'brasero'
  'exiv2'
  'glib2-devel'
  'itstool'
  'libraw'
  'libtiff'
  'meson'
)
optdepends=(
  'brasero: Burn discs'
  'exiv2: Embedded metadata support'
  'libgphoto2: Digital camera support'
  'libjpeg-turbo: JPEG writing support'
  'libraw: Support for RAW photos'
  'libtiff: TIFF writing support'
  'yelp: View help'
)
source=("$pkgname-$pkgver.tar.gz::$url/archive/$pkgver.tar.gz")
sha256sums=('d0fc194e404a199518ca3f507468788041162ce31286aa45dff56df15635d44d')

build() {
  arch-meson "$pkgname-$pkgver" build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs

  desktop-file-validate build/data/*.desktop
}

package() {
  meson install -C build --no-rebuild --destdir "$pkgdir"
}
